package com.physion.util;

public class Constants {
    public static final String BASE_API_PATH = "/physion-api";
    public static final String API_VERSION_PATH = "/v1";
    public static final String BARCODES_PATH = "/barcodes";
}
