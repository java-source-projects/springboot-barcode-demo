package com.physion.service.mapper;

import com.physion.persistance.entity.Dosimetro;
import com.physion.service.mapper.dto.DosimetroDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", imports = {Dosimetro.class, DosimetroDTO.class})
public interface DosimetroMapper {

    DosimetroMapper MAPPER = Mappers.getMapper(DosimetroMapper.class);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "barcode", source = "barcode")
    @Mapping(target = "uploadDate", source = "uploadDate")
    DosimetroDTO mapToDto(Dosimetro entity);
}
