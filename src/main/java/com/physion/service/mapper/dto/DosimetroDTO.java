package com.physion.service.mapper.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"id", "barcode", "uploadDate"})
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DosimetroDTO {
    private Integer id;
    private String barcode;
    private Date uploadDate;
}
