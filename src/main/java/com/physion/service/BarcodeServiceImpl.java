package com.physion.service;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.physion.exceptions.ServiceException;
import com.physion.persistance.entity.Dosimetro;
import com.physion.persistance.repository.DosimetroRepository;
import com.physion.service.mapper.DosimetroMapper;
import com.physion.service.mapper.dto.DosimetroDTO;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BarcodeServiceImpl implements BarcodeService {

    private final DosimetroRepository dosimetroRepository;

    public BarcodeServiceImpl(DosimetroRepository dosimetroRepository) {
        this.dosimetroRepository = dosimetroRepository;
    }

    @Override
    public List<DosimetroDTO> findAll() {
        List<Dosimetro> categories = dosimetroRepository.findAll();
        return categories.stream().map(DosimetroMapper.MAPPER::mapToDto).collect(Collectors.toList());
    }

    @Override
    public DosimetroDTO findByBarcode(MultipartFile multipartFile) throws IOException, ChecksumException, NotFoundException, FormatException {
        InputStream qrInputStream = multipartFile.getInputStream();
        BufferedImage qrBufferedImage = ImageIO.read(qrInputStream);

        LuminanceSource source = new BufferedImageLuminanceSource(qrBufferedImage);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        Reader reader = new MultiFormatReader();
        Result stringBarCode = reader.decode(bitmap);

        Optional<Dosimetro> optionalDosimetro = dosimetroRepository.findByBarcode(stringBarCode.getText());

        if(optionalDosimetro.isPresent()){
            return DosimetroMapper.MAPPER.mapToDto(optionalDosimetro.get());
        }

        throw new ServiceException("Not Found", HttpStatus.NOT_FOUND.value());
    }

}
