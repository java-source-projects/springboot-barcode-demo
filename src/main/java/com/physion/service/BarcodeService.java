package com.physion.service;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.physion.service.mapper.dto.DosimetroDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface BarcodeService {
    List<DosimetroDTO> findAll();

    DosimetroDTO findByBarcode(MultipartFile multipartFile) throws IOException, ChecksumException, NotFoundException, FormatException;
}
