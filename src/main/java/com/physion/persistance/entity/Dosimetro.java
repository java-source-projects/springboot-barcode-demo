package com.physion.persistance.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "dosimetro")
@Data
public class Dosimetro {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "barcode")
    private String barcode;
    @Column(name = "upload_date")
    private Date uploadDate;
}
