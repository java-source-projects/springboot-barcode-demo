package com.physion.persistance.repository;

import com.physion.persistance.entity.Dosimetro;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DosimetroRepository extends JpaRepository<Dosimetro, Integer> {
    Optional<Dosimetro> findByBarcode(String text);
}
