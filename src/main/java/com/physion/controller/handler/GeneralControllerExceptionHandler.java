package com.physion.controller.handler;

import com.physion.controller.dto.DataDTO;
import com.physion.exceptions.ServiceException;
import com.physion.service.mapper.dto.ErrorDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;

@RestControllerAdvice
public class GeneralControllerExceptionHandler {

    @ExceptionHandler(value = {Exception.class})
    @ResponseBody
    public ResponseEntity<DataDTO<ErrorDTO>> handleException(Exception e, HttpServletResponse response) {
        ErrorDTO errorDTO = new ErrorDTO();
        errorDTO.setErrorMessage(e.getMessage());
        DataDTO<ErrorDTO> dataDTO = new DataDTO<>();
        dataDTO.setData(errorDTO);
        dataDTO.setCode(Integer.toString(HttpStatus.INTERNAL_SERVER_ERROR.value()));
        dataDTO.setMessage(e.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(dataDTO);
    }

    @ExceptionHandler(value = {ServiceException.class})
    @ResponseBody
    public ResponseEntity<DataDTO<ErrorDTO>> handleServiceException(ServiceException e, HttpServletResponse response) {
        ErrorDTO errorDTO = new ErrorDTO();
        errorDTO.setErrorMessage(e.getMessage());
        DataDTO<ErrorDTO> dataDTO = new DataDTO<>();
        dataDTO.setData(errorDTO);
        dataDTO.setCode(e.getCode().toString());
        dataDTO.setMessage(e.getMessage());
        return ResponseEntity.status(e.getCode()).body(dataDTO);
    }

}
