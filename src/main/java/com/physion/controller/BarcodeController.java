package com.physion.controller;

import com.physion.controller.dto.DataDTO;
import com.physion.exceptions.ServiceException;
import com.physion.service.BarcodeService;
import com.physion.service.BarcodeServiceImpl;
import com.physion.service.mapper.dto.DosimetroDTO;
import com.physion.util.Constants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping(value = Constants.BASE_API_PATH + Constants.API_VERSION_PATH)
@Validated
public class BarcodeController {

    private final BarcodeService barcodeService;

    public BarcodeController(BarcodeServiceImpl barcodeService) {
        this.barcodeService = barcodeService;
    }

    @GetMapping(Constants.BARCODES_PATH)
    @ResponseBody
    public ResponseEntity<List<DosimetroDTO>> findAll() {
        return ResponseEntity.ok().body(barcodeService.findAll());
    }

    @PostMapping(Constants.BARCODES_PATH)
    public ResponseEntity<DataDTO<DosimetroDTO>> uploadImage(@RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            throw new ServiceException("File cant not by empty!", HttpStatus.BAD_REQUEST.value());
        }

        try {
            DosimetroDTO dosimetroDTO = barcodeService.findByBarcode(file);
            DataDTO<DosimetroDTO> data = new DataDTO<>();
            data.setData(dosimetroDTO);
            data.setCode(Integer.toString(HttpStatus.OK.value()));
            data.setMessage(HttpStatus.OK.getReasonPhrase());
            return ResponseEntity.status(HttpStatus.OK).body(data);
        } catch (Exception exception) {
            if(exception instanceof ServiceException){
                throw new ServiceException(exception.getMessage(), ((ServiceException) exception).getCode());
            }
            throw new ServiceException(HttpStatus.BAD_REQUEST.toString(), HttpStatus.BAD_REQUEST.value());
        }
    }
}
